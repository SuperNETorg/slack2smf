<?php

use Frlnc\Slack\Http\SlackResponseFactory;
use Frlnc\Slack\Http\CurlInteractor;
use Frlnc\Slack\Core\Commander;

class SlackApi
{
    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    static public function init()
    {
        $slackConfig = require __DIR__ . '/../config/slack.php';
        return new SlackApi($slackConfig['token']);
    }

    public function getChannelHistory($channel, $latest = null, $oldest = 0, $count = 1000)
    {
        $parametes = array(
            'channel' => $channel,
            'oldest' => $oldest,
            'count' => $count
        );

        if ($latest)
        {
            $parametes['latest'] = $latest;
        }

        $response = $this->run('channels.history', $parametes);
        return $response;
    }

    public function getUsers()
    {
        $response = $this->run('users.list');
        return $response['members'];
    }

    public function getChannels()
    {
        $response = $this->run('channels.list');
        return $response['channels'];
    }

    public function run($command, $parameters = array())
    {
        $interactor = new CurlInteractor;
        $interactor->setResponseFactory(new SlackResponseFactory);

        $commander = new Commander($this->token, $interactor);

        $response = $commander->execute($command, $parameters)->getBody();

        if (!@$response['ok'])
        {
            throw new Exception('Response fail: ' . @$response['error']);
        }

        return $response;
    }
}