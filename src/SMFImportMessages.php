<?php

use Illuminate\Database\Capsule\Manager as DB;

class SMFImportMessages
{
    /** @var DB_Message[] */
    protected $messagesBuffer = array();
    protected $messageTextBuffer = '';

    protected $config;

    protected $messageDay;

    /** @var SMFApi_Abstract */
    protected $api;

    /** @var DateTimeZone */
    protected $timezone;

    public function __construct(SMFApi_Abstract $api, $config, DateTimeZone $timezone)
    {
        $this->api = $api;
        $this->config = $config;
        $this->timezone = $timezone;
    }

    public function run(DB_Channel $channel)
    {
        /** @var DB_Message[] $messages */
        $messages = DB_Message::with('userObj')->where('channel', $channel->id)->orderBy('ts', 'ASC')->get();

        if (sizeof($messages) == 0)
        {
            return 0;
        }

        $this->messageTextBuffer = '';
        $this->messagesBuffer = array();

        foreach ($messages as $message)
        {
            $this->addMessage($message, $channel->smf_topic_id);
        }

        $this->flushMessages($channel->smf_topic_id);

        return sizeof($messages);
    }

    protected function addMessage(DB_Message $message, $topicId)
    {
        $msgText = $this->messageFormat($message, (strlen($this->messageTextBuffer) > 0));

        $dt = DateTime::createFromFormat('U', floor($message->ts), $this->timezone);
        $messageDay = $dt->format('d.m.Y');

        // Split messages
        if (strlen($this->messageTextBuffer) + strlen($msgText) > SMFApi_Abstract::MAX_POST_SIZE // by max SMF post size
            || ($this->messageDay !== null && $this->messageDay != $messageDay) // by day
        )
        {
            $this->flushMessages($topicId);
        }

        $this->messageDay = $messageDay;
        $this->messageTextBuffer .= $msgText;
        $this->messagesBuffer[] = $message;
    }

    protected function messageFormat(DB_Message $message, $prependSeparator = false)
    {
        $text = $message->text;

        // Replace <@U036ZDXSF|viorcoin> or <@U036ZDXSF> to usernames
        while (preg_match('/<@(\w+)(?:\|(.*?))?>/', $text, $m))
        {
            if (isset($m[2]))
            {
                $userName = $m[2];
            }
            else
            {
                $userId = $m[1];
                $user = DB_User::find($userId);
                $userName = $user ? $user->name : $userId;

            }
            $text = str_replace($m[0], $userName, $text);
        }

        // Replace links
        while (preg_match('/<((?:http|mailto|ftp).*?)>/i', $text, $m))
        {
            $text = str_replace($m[0], '[url]'.$m[1].'[/url]', $text);
        }

        $msgTime = DateTime::createFromFormat('U', floor($message->ts), $this->timezone);

        $msgText = $this->config['message_template'];
        $msgText = str_replace('{USERNAME}', $message->userObj->name, $msgText);
        $msgText = str_replace('{MESSAGE}', $text, $msgText);
        $msgText = str_replace('{DATE}', $msgTime->format($this->config['date_format']), $msgText);

        if ($prependSeparator)
        {
            $msgText = $this->config['message_separator'] . PHP_EOL . $msgText;
        }

        return $msgText;
    }


    protected function flushMessages($topicId)
    {
        if (sizeof($this->messagesBuffer) == 0)
        {
            return;
        }

        $this->postMessage($this->messageTextBuffer, $topicId);

        DB::connection()->beginTransaction();
        foreach ($this->messagesBuffer as $m)
        {
            $m->delete();
        }
        DB::connection()->commit();

        $this->messageTextBuffer = '';
        $this->messagesBuffer = array();

    }

    protected function postMessage($text, $topicId)
    {
        $msgOptions = array(
            'subject' => $this->messageDay,
            'body' => $text
        );
        $topicOptions = array(
            'board' => $this->config['board_id'],
            'id' => $topicId
        );
        $posterOptions = array();

        $result = $this->api->createPost($msgOptions, $topicOptions, $posterOptions);

        if (!$result)
        {
            throw new Exception('SMF message post fail');
        }
    }
}