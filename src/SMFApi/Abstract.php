<?php

abstract class SMFApi_Abstract
{
    const MAX_POST_SIZE = 65535;

    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    abstract public function createPost($msgOptions, $topicOptions, $posterOptions);

    /**
     * @param array $config
     * @return SMFApi_Abstract
     */
    static public function factory(array $config)
    {
        $class = 'SMFApi_' . ucfirst($config['class']);

        return new $class($config);
    }
}