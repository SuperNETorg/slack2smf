<?php

class SMFApi_Remote extends SMFApi_Abstract
{
    public function createPost($msgOptions, $topicOptions, $posterOptions)
    {
        $params = array(
            'msg' => $msgOptions,
            'topic' => $topicOptions,
            'poster' => $posterOptions
        );
        $postFields = 'key=' . urlencode($this->config['remote']['api_key'])
            . '&method=create_post'
            . '&params=' . urlencode(json_encode($params));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config['remote']['url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $rawData = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($rawData === false)
        {
            throw new Exception('API request fail: ' . curl_error($ch));
        }
        curl_close($ch);

        $result = @json_decode($rawData, true);
        if (!is_array($result) || @$result['status'] != 'success' || !isset($result['result']))
        {
            throw new Exception('API request fail: ' . @$info['http_code'] . ' ' . $rawData);
        }

        return $result['result'];
    }
}