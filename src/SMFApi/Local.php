<?php

class SMFApi_Local extends SMFApi_Abstract
{
    public function createPost($msgOptions, $topicOptions, $posterOptions)
    {
        require_once $this->config['local']['path'] . '/SSI.php';
        global $sourcedir;
        require_once($sourcedir . '/Subs-Post.php');

        if (!isset($topicOptions['board']) || !isset($msgOptions['subject']) || !isset($msgOptions['body'])) {
            return false;
        }

        $result = createPost($msgOptions, $topicOptions, $posterOptions);
        if ($result)
        {
            return array(
                'msg' => $msgOptions,
                'topic' => $topicOptions,
                'poster' => $posterOptions
            );
        }
        else
        {
            return false;
        }
    }

}