<?php

/**
 * @property $id
 * @property $name
 * @property $deleted
 * @property $first_name
 * @property $last_name
 * @property $real_name
 * @property $email
 * @property $skype
 * @property $phone
 */
class DB_User extends Illuminate\Database\Eloquent\Model
{
    protected $table = "users";
    public $timestamps = false;
}