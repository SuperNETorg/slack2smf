<?php

/**
 * @property $id
 * @property $text
 * @property $ts
 * @property $type
 * @property $user
 * @property $channel
 *
 * @property $userObj DB_User
 */
class DB_Message extends Illuminate\Database\Eloquent\Model
{
    protected $table = "messages";
    public $timestamps = false;

    protected $fillable = array('text', 'ts', 'type', 'user', 'channel');

    public function userObj()
    {
        return $this->hasOne('DB_User', 'id', 'user');
    }
}