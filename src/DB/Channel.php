<?php

/**
 * @property $id
 * @property $name
 * @property $smf_topic_id
 * @property $last_message_read
 */
class DB_Channel extends Illuminate\Database\Eloquent\Model
{
    protected $table = "channels";
    public $timestamps = false;

    protected $attributes = array(
        // default values
        'last_message_read' => 0,
    );
}