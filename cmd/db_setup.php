<?php

require __DIR__ . '/../init.php';
$dbConfig = require __DIR__ . '/../config/db.php';

use Illuminate\Database\Capsule\Manager as Capsule;

// Users table
Capsule::schema()->dropIfExists('users');
Capsule::schema()->create('users', function($table)
{
    /* @var $table \Illuminate\Database\Schema\Blueprint */
    $table->string('id', 20)->primary('id');
    $table->string('name');
    $table->integer('deleted')->default(0);
    $table->string('first_name')->nullable();
    $table->string('last_name')->nullable();
    $table->string('real_name')->nullable();
    $table->string('email')->nullable();
    $table->string('skype')->nullable();
    $table->string('phone')->nullable();

});

// Channels table
Capsule::schema()->dropIfExists('channels');
Capsule::schema()->create('channels', function($table)
{
    /* @var $table \Illuminate\Database\Schema\Blueprint */
    $table->string('id', 20)->primary('id');
    $table->string('name');
    $table->integer('smf_topic_id')->nullable();
    $table->string('last_message_read', 30);
});

// Message list table
Capsule::schema()->dropIfExists('messages');
Capsule::schema()->create('messages', function($table)
{
    /* @var $table \Illuminate\Database\Schema\Blueprint */
    $table->bigIncrements('id');
    $table->string('channel', 20);
    $table->string('type', 20);
    $table->double('ts');
    $table->string('user', 20);
    $table->text('text');

    $table->index('channel');
});
