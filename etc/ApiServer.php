<?php

define('API_SECRET_KEY', 'nij834u8jh903478h2f389hfdf0h93f');
define('SMF_PATH', '/var/www/smf');

if (@$_POST['key'] != API_SECRET_KEY)
{
    echo response('error', 'wrong api key');
    exit;
}

$params = json_decode($_POST['params'], true);

if ($_POST['method'] == 'create_post')
{
    $result = _createPost($params['msg'], $params['topic'], $params['poster']);
    echo response($result ? 'success' : 'error', $result);
    exit;
}
else
{
    echo response('error', 'wrong method');
    exit;
}

// *************************************************************

function response($status, $result = null)
{
    return json_encode(array(
        'status' => $status,
        'result' => $result
    ));
}

function _createPost($msgOptions, $topicOptions, $posterOptions)
{
    require_once SMF_PATH . '/SSI.php';
    global $sourcedir;
    require_once($sourcedir . '/Subs-Post.php');

    if (!isset($topicOptions['board']) || !isset($msgOptions['subject']) || !isset($msgOptions['body'])) {
        return false;
    }

    $result = createPost($msgOptions, $topicOptions, $posterOptions);
    if ($result)
    {
        return array(
            'msg' => $msgOptions,
            'topic' => $topicOptions,
            'poster' => $posterOptions
        );
    }
    else
    {
        return false;
    }
}