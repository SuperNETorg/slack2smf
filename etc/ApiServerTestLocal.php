<?php

error_reporting(E_ALL);


/*
 * Test if SMF posting is working.
 * Copy this file to a SMF server and run it from console.
 * It must create a new topic 'Test' in board 1.
 */

// Put actual SMF path here
define('SMF_PATH', '/var/www/smf');

if (!file_exists(SMF_PATH . '/SSI.php'))
{
    echo "Wrong SMF path" . PHP_EOL;
    die;
}

$msgOptions = array(
    'subject' => 'Test',
    'body' => 'Test'
);
$topicOptions = array(
    'board' => 1,
);
$posterOptions = array();

$result = _createPost($msgOptions, $topicOptions, $posterOptions);
var_dump($result);

// *************************************************************

function _createPost($msgOptions, $topicOptions, $posterOptions)
{
    require_once SMF_PATH . '/SSI.php';
    global $sourcedir;
    require_once($sourcedir . '/Subs-Post.php');

    if (!isset($topicOptions['board']) || !isset($msgOptions['subject']) || !isset($msgOptions['body'])) {
        return false;
    }

    $result = createPost($msgOptions, $topicOptions, $posterOptions);
    if ($result)
    {
        return array(
            'msg' => $msgOptions,
            'topic' => $topicOptions,
            'poster' => $posterOptions
        );
    }
    else
    {
        return false;
    }
}