<?php

require __DIR__ . '/vendor/autoload.php';
$dbConfig = require __DIR__ . '/config/db.php';

use Illuminate\Database\Capsule\Manager as DBManager;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$capsule = new DBManager;
$capsule->addConnection($dbConfig);

// Set the event dispatcher used by Eloquent models... (optional)
//$capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

$capsule->bootEloquent();
