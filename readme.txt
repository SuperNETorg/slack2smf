=== Requirements ===

 * PHP >= 5.4
 * PHP extensions: curl, mysql
 * MySQL

 * SMF 2.* (installed locally)

=== Setup ===

 1 Install packages via composer (php composer.phar install)
 2 Fill config files (config/*.php)
    - If remote posting is used, fill 'API_SECRET_KEY' AND 'SMF_PATH' parameters and copy 'etc/ApiServer.php' to SMF server
 3 Setup DB structure (run "php cmd/db_setup.php")
 4 Add cron jobs:
    # Fetching new messages from Slack (example: every 4 hours)
    0 */4 * * * php /var/www/html/app/cmd/fetch.php >> /var/log/slack_fetch.log 2>&1
    # Posting messages to SMF (example: once a day at 00:15)
    15 0 * * * php /var/www/html/app/cmd/smf_import.php >> /var/log/slack_export.log 2>&1
