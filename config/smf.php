<?php

return array(
    'message_template' => '[{DATE}] <[b]{USERNAME}[/b]> {MESSAGE}', //'[b]{USERNAME}[/b] [i]{DATE}[/i]' . PHP_EOL . '{MESSAGE}' . PHP_EOL,
    'message_separator' => '', // '[hr]',
    'date_format' => 'H:i',
    'board_id' => 1,

    'class' => 'remote', // local | remote

    // if 'class' is 'local'
    'local' => array(
        'path' => '/var/www/smf',
    ),

    // if 'class' is 'remote'
    'remote' => array(
        'url' => 'http://localhost/slack_export/docs/ApiServer.php',
        'api_key' => 'nij834u8jh903478h2f389hfdf0h93f'
    )
);